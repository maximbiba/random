package com.company;

public class Randomizer {
    public int getRandomNumber(int min, int max){
        return (int) (Math.random() * (max - min + 1)) + min;
    }
    public void printRandomNumber(int min, int max, int nTimes){
        for(int i = 0; i < nTimes; i++){
            System.out.println(getRandomNumber(min, max));
        }
    }
}
