package com.company.test;
import com.company.Randomizer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RandomizerTest {
    Randomizer randomizer = new Randomizer();

    @Test
    void getRandomNumberTest1() {
        boolean actual = true;
        int min = -10;
        int max = 10;
        int result = 0;
        for (long i = 0l; i < 1_000_000l; i++) {
            result = randomizer.getRandomNumber(min,max);
            if(result < min || result > max){
                actual = false;
            }
        }
        assertTrue(actual);
    }
}


