package com.company;

public class Main {

    public static void main(String[] args) {
	Randomizer randomizer = new Randomizer();
        randomizer.printRandomNumber(0, 10, 1);
        System.out.println("************************");

        randomizer.printRandomNumber(1, 6, 10);
        System.out.println("************************");

        randomizer.printRandomNumber(0, 10, 10);
        System.out.println("************************");

        randomizer.printRandomNumber(20, 50, 10);
        System.out.println("************************");


        randomizer.printRandomNumber(-10, 10, 10);
        System.out.println("************************");


        randomizer.printRandomNumber(-10, 35, randomizer.getRandomNumber(3,15));
        System.out.println("************************");

    }
}



